import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'

// @ts-ignore
import Icon from 'vue-awesome/components/Icon'
import 'vue-awesome/icons/cog'
import 'vue-awesome/icons/eye'
import 'vue-awesome/icons/exclamation-triangle'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.component('v-icon', Icon)
Vue.use(BootstrapVue)

Vue.config.productionTip = false

new Vue({
  render: (h) => h(App),
}).$mount('#app')
