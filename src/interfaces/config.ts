export interface Config {
  url: string,
  token: string,
  groups: string,
  projects: string,
}
