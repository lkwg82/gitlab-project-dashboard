![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/jiaan/gitlab-pipeline-dashboard.svg)
![NPM](https://img.shields.io/npm/v/gitlab-pipeline-dashboard.svg)
![NPM](https://img.shields.io/npm/l/gitlab-pipeline-dashboard.svg)

# Gitlab Pipeline Dashboard

This is a dashboard for [GitLab's CI](https://about.gitlab.com/gitlab-ci/) (continuous integration) pipelines! Best viewed on a large display such as an TV.

[Try the demo dashboard.](https://jiaan.gitlab.io/gitlab-pipeline-dashboard/)

![preview](https://gitlab.com/jiaan/gitlab-pipeline-dashboard/raw/master/resources/preview.jpg)

This is my first release of the project and I am embarrassed by the sloppy code. So please [report](https://gitlab.com/jiaan/gitlab-pipeline-dashboard/issues) any issues you find! 🕵

## Quick start

Install
```bash
# Using yarn
yarn global add gitlab-pipeline-dashboard

# Using NPM
npm i -g gitlab-pipeline-dashboard
```

Run
```bash
ci-dashboard
```

To specify another port
```bash
PORT=8081 ci-dashboard
```

## Description

I think most of us can agree that GitLab CI is awesome, but unfortunately it doesn't ship with a dashboard (yet).

So here is my solution! 😎

_What makes it diffferent from other solutions out there?_

This dashboard scans all the projects of your group(s) for active pipelines. Thus you don't need to reconfigure it everytime you add CI to your projects. 👌

It was inspired by [this issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/3235).

![mockup](https://camo.githubusercontent.com/13eeecb60b49647c64668ff43edc8042b4fceecd/68747470733a2f2f6769746c61622e636f6d2f6769746c61622d6f72672f6769746c61622d63652f75706c6f6164732f32626638353064656537303736376263346461633437663764363035646664302f417274626f6172645f315f436f70795f332e706e67)

## Development

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
