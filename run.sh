#!/bin/bash

set -euo pipefail

docker run \
  --rm \
  -ti \
  --entrypoint /bin/bash \
  -v "$PWD:/src" \
  -w /src \
  -p 8080:8080 \
  node:15